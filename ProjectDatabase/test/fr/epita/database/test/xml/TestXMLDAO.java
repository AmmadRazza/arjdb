package fr.epita.database.test.xml;

import java.util.List;

import fr.epita.database.datamodel.Identity;
import fr.epita.database.exceptions.EntityCreationException;
import fr.epita.database.exceptions.EntityDeletionException;
import fr.epita.database.exceptions.EntitySearchException;
import fr.epita.database.exceptions.EntityUpdateException;
import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.services.identity.IdentityDAO;
import fr.epita.database.services.identity.IdentityXMLDAO;

public class TestXMLDAO 
{
	public static void main(String[] args) 
	{
		IdentityDAO dao = null;
		
		try 
		{
			dao = new IdentityXMLDAO();
		}
		catch (Exception e) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
			e.printStackTrace();
		}
		
		final Identity ammad = new Identity("Ammad", "Raza", "ammad@raza.com");
		final Identity raza = new Identity("Raza", "Raza", "raza@ammad.com");
		
		try 
		{
			dao.create(ammad);
		}
		catch (EntityCreationException e) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
			e.printStackTrace();
		}
		
		List<Identity> resultList = null;
		try 
		{
			resultList = dao.search(ammad, ConfKey.SEARCH_BY_UID);
		}
		catch (EntitySearchException e) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
			e.printStackTrace();
		}
		
		// then
		if (resultList.size() <= 0) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
		}
		else 
		{
			System.out.println(resultList);
			System.out.println("search successful!");
		}
				
		//will replace first identity with second
		try 
		{
			dao.update(ammad, raza);
		}
		catch (EntityUpdateException e) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
			e.printStackTrace();
		}
		
		try 
		{
			dao.delete(ammad);
		}
		catch (EntityDeletionException e) 
		{
			System.out.println("There has been some problem with the database\nPlease Try Again Later!");
			e.printStackTrace();
		}

	}

}
