/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant :
 */
package fr.epita.database.test.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.services.conf.ConfigurationService;

/**
 * <h3>Description</h3>
 * <p>This class was used for testing properties</p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public class TestProperties 
{

	public static void main(String[] args) throws FileNotFoundException, IOException 
	{
		final Properties properties = new Properties();
		
		properties.load(new FileInputStream(new File("src/test.properties")));
		
		final Set<Object> keySet = properties.keySet();
		
		System.out.println(properties.getProperty("db.user"));
		
		System.out.println(keySet);
		
		System.out.println(ConfigurationService.getProperty(ConfKey.DB_USER));
	}

}
