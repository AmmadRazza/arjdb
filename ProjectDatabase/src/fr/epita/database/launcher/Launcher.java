/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant :
 */
package fr.epita.database.launcher;

import fr.epita.database.services.identity.MainPanel;

/**
 * <h3>Description</h3>
 * <p>
 * This class will start the program in the console
 * </p>
 *
 * <h3>Usage</h3>
 * <p>
 * This class should be used as follows:
 *
 * <pre>
 * <code>${type_name} instance = new ${type_name}();</code>
 * </pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 *         ${tags}
 */
public class Launcher 
{
	public static void main(String[] args) throws Exception 
	{
		final MainPanel databaseSystem = new MainPanel();
		
		if(databaseSystem.isLoginSuccessful() == false) 
			return;
		
		databaseSystem.displayDatabaseOptions();
		databaseSystem.ActivateDatabase();	
	}

}
