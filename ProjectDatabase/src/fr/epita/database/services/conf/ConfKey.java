/**
 * Ce fichier est la propriété de Thomas BROUSSARD
 * Code application :
 * Composant :
 */
package fr.epita.database.services.conf;

/**
 * <h3>Description</h3>
 * <p>This class allows developer to develop a secure code, by letting enum hold the values, and organize the code in a more better form</p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public enum ConfKey
{
	/**
	 * this is the key to choose the backend mode
	 */
	BACKEND_MODE("backend.mode"),
	/**
	 * this is the key to choose the fall back backend mode
	 */
	FALLBACK_BACKEND_MODE("backend.mode"),

	/**
	 * Main User Name to login
	 */
	DB_USER("db.user"),

	/**
	 * Main Password to login
	 */
	DB_PASSWORD("db.pwd"),

	/**
	 *	Used to search identity in the database
	 */
	IDENTITY_SEARCH_QUERY(
			"identity.search"),
	/**
	 *	Used to insert identity in the database
	 */
	IDENTITY_INSERT_QUERY("identity.insert"),

	/**
	 *	gives the path to XML file
	 */
	XML_BACKEND_FILE("xml.file"),

	/**
	 *	Used in search option to search by the name 
	 */
	SEARCH_BY_NAME("1"),
	
	/**
	 *	Used in search option to search by User Id 
	 */
	SEARCH_BY_UID("2"),
	
	/**
	 *	Used in search option to search by the email
	 */
	SEARCH_BY_EMAIL("3"),
	
	/**
	 *	Used in search option to search by both Name and User ID
	 */
	SEARCH_BY_NAME_AND_UID("4"),
	
	
	;

	private String key;

	/**
	 * gets the value of the variable contain in enum
	 */
	private ConfKey(String key) 
	{
		this.key = key;
	}

	/*
	 * returns the value
	 * */
	public String getKey() 
	{
		return key;
	}

}
