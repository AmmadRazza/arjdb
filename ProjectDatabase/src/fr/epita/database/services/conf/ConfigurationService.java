/**
 * Ce fichier est la propriété de Ammad Raza Code application : Composant :
 */
package fr.epita.database.services.conf;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * <h3>Description</h3>
 * <p>
 * This class allows to get data from properties file in a secure manner.
 * </p>
 *
 * <h3>Usage</h3>
 * <p>
 * This class should be used as follows:
 *
 * <pre>
 * <code>${type_name} instance </code>
 * </pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 *         ${tags}
 */
public class ConfigurationService 
{
	protected static Properties properties;

	static 
	{
		init();
	}

	private static void init() 
	{
		try 
		{
			properties = new Properties();
			properties.load(new FileInputStream(new File("src/myDatabase.properties")));					
		}
		catch (final Exception e) 
		{
			System.out.println("There has been some problem with the data\nPlease Try Again Later!");
			e.printStackTrace();
		}
	}

	public static Integer getIntProperty(ConfKey key) 
	{
		final String valueAsString = getProperty(key);
		return Integer.valueOf(valueAsString);
	}

	public static String getProperty(ConfKey key) 
	{
		return properties.getProperty(key.getKey());
	}

}
