/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant :
 */
package fr.epita.database.services;

import java.io.Serializable;
import java.util.List;

import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.datamodel.Identity;
import fr.epita.database.exceptions.EntityCreationException;
import fr.epita.database.exceptions.EntityDeletionException;
import fr.epita.database.exceptions.EntityReadException;
import fr.epita.database.exceptions.EntitySearchException;
import fr.epita.database.exceptions.EntityUpdateException;

/**
 * <h3>Description</h3>
 * <p>This class is a collection of abstract methods.
 * A class implements an interface, thereby inheriting the abstract methods of the interface.
 * </p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public interface DAO<T> 
{
	public void create(T entity) throws EntityCreationException;

	public void delete(T entity) throws EntityDeletionException;

	public void update(T entity, T newEnity) throws EntityUpdateException;

	public Identity getById(Serializable id) throws EntityReadException;

	public List<Identity> search(T criteria, ConfKey key) throws EntitySearchException;

}
