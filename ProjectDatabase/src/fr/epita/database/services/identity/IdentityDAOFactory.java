/**
 * Ce fichier est la propriété de Ammad Raza Code application : Composant :
 */
package fr.epita.database.services.identity;

import java.util.function.BiPredicate;

import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.services.conf.ConfigurationService;
import fr.epita.database.datamodel.Identity;

/**
 * <h3>Description</h3>
 * <p>
 * This class is using factory design pattern, 
 * that allow the developer to switch between different backend database.
 * </p>
 *
 * <h3>Usage</h3>
 * <p>
 * This class should be used as follows:
 *
 * <pre>
 * <code>${type_name} instance = new ${type_name}();</code>
 * </pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 *         ${tags}
 */
public class IdentityDAOFactory 
{
	private static IdentityDAO currentInstance;

	private static boolean fallbackActivated;

	public static IdentityDAO getDAO() throws Exception 
	{	
		final String backendMode = ConfigurationService.getProperty(ConfKey.BACKEND_MODE);
		
		if (currentInstance == null) 
		{
			currentInstance = getInstance(backendMode);
		}
		if (currentInstance != null && !currentInstance.healthCheck()) 
		{
			fallbackActivated = true;
			final String fallbackMode = ConfigurationService.getProperty(ConfKey.FALLBACK_BACKEND_MODE);
			currentInstance = getInstance(fallbackMode);
		}

		return currentInstance;
	}

	/**
	 * <h3>Description</h3>
	 * <p>
	 * This method will check which type of Database is been used, and will proceed accordingly
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>
	 * It should be used as follows :
	 *
	 * <pre>
	 * <code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code>
	 * </pre>
	 * </p>
	 *
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 *         ${tags}
	 */
	private static IdentityDAO getInstance(final String backendMode) throws Exception
	{
		IdentityDAO instance = null;
		
		switch (backendMode) 
		{
		
		case "db":
//			instance = new IdentityJDBCDAO(); //Since we are not using JDBC there is no need of this instance
			break;
		case "file":	//Since we are not using file there is no need of this option as well
//			try 
//			{
//				instance = new FileIdentityDAO(new BiPredicate<Identity, Identity>() 
//				{
//
//					@Override
//					public boolean test(Identity identity1, Identity identity2) 
//					{
//						return identity1.getEmail().startsWith(identity2.getEmail())
//								|| identity1.getDisplayName().startsWith(identity2.getDisplayName());
//					}
//				});
//			} 
//			catch (final Exception e) 
//			{
//			}
			break;
		case "xml":
			instance = new IdentityXMLDAO();
			break;
		default:
			throw new Exception("not implemented yet");
		}
		return instance;

	}

}
