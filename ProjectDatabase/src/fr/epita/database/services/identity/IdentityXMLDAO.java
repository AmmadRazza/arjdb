/**
 * Ce fichier est la propriété de Ammad Raza Code application : Composant :
 */
package fr.epita.database.services.identity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.epita.database.datamodel.Identity;
import fr.epita.database.exceptions.DocXMLException;
import fr.epita.database.exceptions.EntityCreationException;
import fr.epita.database.exceptions.EntityDeletionException;
import fr.epita.database.exceptions.EntityReadException;
import fr.epita.database.exceptions.EntitySearchException;
import fr.epita.database.exceptions.EntityUpdateException;
import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.services.conf.ConfigurationService;

/**
 * <h3>Description</h3>
 * <p>
 * This class allows to create, search, update and remove identities from the database
 * </p>
 *
 * <h3>Usage</h3>
 * <p>
 * This class should be used as follows:
 *
 * <pre>
 * <code>${type_name} instance = new ${type_name}();</code>
 * </pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 *         ${tags}
 */
public class IdentityXMLDAO implements IdentityDAO 
{
	private Document document;
	
	/** 
	 * <h3>Description</h3>  
	 * <p>Constructor</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	public IdentityXMLDAO() throws Exception
	{
		document = null;

		try 
		{
			document = getDocument();
		} 
		catch (ParserConfigurationException | SAXException | IOException e) 
		{
			throw new DocXMLException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.epita..services.DAO#create(java.lang.Object)

	 */
	
	/** 
	 * <h3>Description</h3>  
	 * <p>Method allows to create and add a new identity to the database</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	@Override
	public void create(Identity entity) throws EntityCreationException 
	{
		Document doc = null;
		try 
		{
			doc = getDocument();
		} 
		catch (ParserConfigurationException | SAXException | IOException e) 
		{
			throw new EntityCreationException(entity, e);
		}
		
		final Node rootNode = doc.getElementsByTagName("identities").item(0);
		final Element newIdentity = doc.createElement("identity");
		final Element displayNameElement = doc.createElement("displayName");
		final Element emailElement = doc.createElement("email");
		final Element uidElement = doc.createElement("uid");

		newIdentity.appendChild(displayNameElement);
		newIdentity.appendChild(emailElement);
		newIdentity.appendChild(uidElement);

		displayNameElement.setTextContent(entity.getDisplayName());
		emailElement.setTextContent(entity.getEmail());
		uidElement.setTextContent(entity.getUid());

		rootNode.appendChild(newIdentity);

		writeDoc(doc);
	}

	/** 
	 * <h3>Description</h3>  
	 * <p>This method allows to write formatted data to an XML file</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void writeDoc(Document doc) 
	{
		try 
		{
			final PrintWriter pw = new PrintWriter(ConfigurationService.getProperty(ConfKey.XML_BACKEND_FILE));
			pw.write(docToString(doc));
			pw.flush();
			pw.close();
		} 
		catch (final FileNotFoundException e) 
		{
			System.out.println("There has been some problem with the data\\nPlease Try Again Later!");
			e.printStackTrace();
		}
	}

	/** 
	 * <h3>Description</h3>  
	 * <p>This method allows to convert doc to string</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private static String docToString(Document doc) 
	{
		try
		{
			final StringWriter sw = new StringWriter();
			final TransformerFactory tf = TransformerFactory.newInstance();
			final Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			transformer.transform(new DOMSource(doc), new StreamResult(sw));
			return sw.toString();
		} 
		catch (final Exception ex) 
		{
			throw new RuntimeException("Error converting to String", ex);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.epita.database.services.DAO#delete(java.lang.Object)
	 */
	@Override
	/** 
	 * <h3>Description</h3>  
	 * <p>This method allows to delete/remove an identity from the database</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public void delete(Identity entity) throws EntityDeletionException  
	{
		Document doc = null;
		try 
		{
			doc = getDocument();
		} 
		catch (ParserConfigurationException | SAXException | IOException e) 
		{
			throw new EntityDeletionException(entity, e);
		}
			
		NodeList listOfNodes = doc.getElementsByTagName("identity");
			
		for (int i = 0; i < listOfNodes.getLength(); i++) 
		{
			Element unecessaryIdentity = (Element) doc.getElementsByTagName("identity").item(i);	

			Element name = (Element)unecessaryIdentity.getElementsByTagName("displayName").item(0);
			Element email = (Element)unecessaryIdentity.getElementsByTagName("email").item(0);
			Element uid = (Element)unecessaryIdentity.getElementsByTagName("uid").item(0);

			String identityName = name.getTextContent();
			String identityEmail = email.getTextContent();
			String identityUID = uid.getTextContent();
	 
			if (isIdentityAvailable(entity, identityName, identityEmail, identityUID) == true) 
			{
				Node parentNode = unecessaryIdentity.getParentNode();
				parentNode.removeChild(unecessaryIdentity);
				parentNode.normalize();  		
			}
		}
			    
		try 
	    {
	    	writeDoc(doc);
		}
		catch (Exception e) 
		{
			System.out.println("There has been some problem with the data\nPlease Try Again Later!");
			e.printStackTrace();   
		}	
	}
	
	/*
	 * Will check if the details matches with certain identity information
	 * */
	private boolean isIdentityAvailable(Identity entity, String name, String email, String uid) 
	{
		return name.equals(entity.getDisplayName())
				&& email.equals(entity.getEmail())
				&& uid.equals(entity.getUid());
	}


	/*
	 * (non-Javadoc)
	 * @see fr.epita.database.services.DAO#update(java.lang.Object)
	 */
	@Override
	/** 
	 * <h3>Description</h3>  
	 * <p>This method allows to update/replace certain detail in certain identity in the database</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public void update(Identity entity, Identity newEntity) throws EntityUpdateException 
	{
		Document doc = null;
		try 
		{
			doc = getDocument();
		} 
		catch (ParserConfigurationException | SAXException | IOException e) 
		{
			throw new EntityUpdateException(entity, e);
		}

		NodeList listOfNodes = doc.getElementsByTagName("identity");
		
		for (int i = 0; i < listOfNodes.getLength(); i++) 
		{	
			Element replaceIdentity = (Element) doc.getElementsByTagName("identity").item(i);
			
			Element name = (Element)replaceIdentity.getElementsByTagName("displayName").item(0);
			Element email = (Element)replaceIdentity.getElementsByTagName("email").item(0);
			Element uid = (Element)replaceIdentity.getElementsByTagName("uid").item(0);
			
			String identityName = name.getTextContent();
			String identityEmail = email.getTextContent();
			String identityUID = uid.getTextContent();
	 		
			//find
			if (isIdentityAvailable(entity, identityName, identityEmail, identityUID) == true) 
			{
				try 
				{
					//get the details fromthe doc
					Node newName = doc.getElementsByTagName("displayName").item(i);
					Node newEmail = doc.getElementsByTagName("email").item(i);
					Node newUID = doc.getElementsByTagName("uid").item(i);		
					
					//Replace with new detials
					newName.setTextContent(newEntity.getDisplayName()); 
					newEmail.setTextContent(newEntity.getEmail()); 
					newUID.setTextContent(newEntity.getUid()); 
				} 
				catch (Exception e) 
				{
					System.out.println("\nSorry, we are unable to locate and update the identity\nPlease, try again!\nOR\ncontact technical support!");
					e.printStackTrace();
				}
							
				//update
				writeDoc(doc);
				docToString(doc);    
			}
	 		
			Node parentNode = replaceIdentity.getParentNode();
			
			parentNode.normalize();	
		}
		try 
		{
			writeDoc(doc);		
		} 
		catch (Exception e) 
		{
			System.out.println("\nSorry, we are unable to locate and update the identity\nPlease, try again!\nOR\ncontact technical support!");
			e.printStackTrace();
		}
	}


	/*
	 * (non-Javadoc)
	 * @see fr.epita.database.services.DAO#getById(java.io.Serializable)
	 */
	@Override
	/** 
	 * <h3>Description</h3>  
	 * <p>This method gets an identity from id</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public Identity getById(Serializable id) throws EntityReadException 
	{
		return new Identity();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.epita.database.services.DAO#search(java.lang.Object)
	 */
	@Override
	/** 
	 * <h3>Description</h3>  
	 * <p>This return method perform a search, and returns list of identities in the database </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public List<Identity> search(Identity criteria, ConfKey key) throws EntitySearchException 
	{
		Document doc = null;
		try 
		{
			doc = getDocument();
		} 
		catch (ParserConfigurationException | SAXException | IOException e) 
		{
			System.out.println("\nSorry, we are unable to locate and update the identity\nPlease, try again!\nOR\ncontact technical support!");
			e.printStackTrace();
		}
		
		String xpathExpression = null;
		
		switch(key)
		{
			case SEARCH_BY_NAME:
				xpathExpression = searchByName(criteria);
				break;
			case SEARCH_BY_UID:
				xpathExpression = searchByUID(criteria);
				break;
			case SEARCH_BY_EMAIL:
				xpathExpression = searchByName(criteria);
				break;
			case SEARCH_BY_NAME_AND_UID:
				xpathExpression = searchByUID(criteria);
				break;				
		}
		
		final List<Element> elements = getElements(doc, xpathExpression);

		final List<Identity> results = new ArrayList<>();

		for (final Element element : elements) 
		{
			final Identity identity = getIdentityFromElement(element);
			results.add(identity);
		}

		return results;
	}
	
	/*
	 * Will return the email and/or the name found in the database
	 * */
	private String searchByName(Identity criteria)
	{
		return "/identities/identity[ ./email/text() = '" + criteria.getEmail()        
		+ "' or contains(./displayName/text(), '" + criteria.getDisplayName()
		+"')]";
	}
	
	/*
	 * Will return the User ID and/or the name found in the database
	 * */
	private String searchByUID(Identity criteria)
	{
		return "/identities/identity[ ./uid/text() = '" + criteria.getUid()      
		+ "' or contains(./displayName/text(), '" + criteria.getDisplayName()
		+ "')]";
	}

	/**
	 * <h3>Description</h3>
	 * <p>
	 * This return method gets an identity in the database
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>
	 * It should be used as follows :
	 *
	 * <pre>
	 * <code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code>
	 * </pre>
	 * </p>
	 *
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 *         ${tags}
	 */
	private Identity getIdentityFromElement(Element item) 
	{
		final Identity identity = new Identity();
		final String displayName = item.getElementsByTagName("displayName").item(0).getTextContent();
		final String email = item.getElementsByTagName("email").item(0).getTextContent();
		final String uid = item.getElementsByTagName("uid").item(0).getTextContent();
		identity.setDisplayName(displayName);
		identity.setEmail(email);
		identity.setUid(uid);
		return identity;
	}

	/**
	 * <h3>Description</h3>
	 * <p>
	 * This return method gets the list of elements available in the database
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>
	 * It should be used as follows :
	 *
	 * <pre>
	 * <code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code>
	 * </pre>
	 * </p>
	 *
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 *         ${tags}
	 */
	private List<Element> getElements(Document doc, String xpathExpr) 
	{
		final XPathFactory xpathFactory = XPathFactory.newInstance();
		
		javax.xml.xpath.XPathExpression expr;
		try
		{
			expr = xpathFactory.newXPath().compile(xpathExpr);
			final NodeList xpathEval = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			final List<Element> results = new ArrayList<>();
			for (int i = 0; i < xpathEval.getLength(); i++)
			{
				results.add((Element) xpathEval.item(i));
			}
			return results;
		} 
		catch (final XPathExpressionException e) 
		{
			System.out.println("\nSorry, we are unable to locate and update the identity\nPlease, try again!\nOR\ncontact technical support!");
			e.printStackTrace();
		}
		
		return new ArrayList<>();
	}

	/**
	 * <h3>Description</h3>
	 * <p>
	 * This return method allows to generate an XML file as database
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>
	 * It should be used as follows :
	 *
	 * <pre>
	 * <code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code>
	 * </pre>
	 * </p>
	 *
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 *         ${tags}
	 */
	private Document getDocument() throws ParserConfigurationException, SAXException, IOException, FileNotFoundException 
	{
		final File file = new File(ConfigurationService.getProperty(ConfKey.XML_BACKEND_FILE)); 
		
		if (!file.exists()) 
		{	
			System.out.println("\nDatabase Not Found!");
			System.out.println("\nCreating New Database..");
			waitForSecond(2);
			file.createNewFile();
		}
		
		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		final DocumentBuilder db = dbf.newDocumentBuilder();
		final Document document = db.parse(new FileInputStream(file));
		return document;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.epita.database.services.identity.IdentityDAO#healthCheck()
	 */
	@Override
	/*
	 * return true when the class of certain database is available
	 * */
	public boolean healthCheck() 
	{
		return true;
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>This method allows put a delay between certain operations </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void waitForSecond(long sec) 
	{
		try 
		{
			TimeUnit.SECONDS.sleep(sec);
		}
		catch (InterruptedException e) 
		{
			System.out.println("Unable to proceed. Please contact technical support");
			e.printStackTrace();
		}		
	}

}
