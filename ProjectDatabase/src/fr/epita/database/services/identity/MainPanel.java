
/**
 * Ce fichier est la propriété de Ammad Raza Code application : Composant :
 */
package fr.epita.database.services.identity;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import fr.epita.database.services.conf.ConfKey;
import fr.epita.database.services.conf.ConfigurationService;
import fr.epita.database.datamodel.Identity;
import fr.epita.database.exceptions.EntityCreationException;
import fr.epita.database.exceptions.EntityDeletionException;

/**
 * <h3>Description</h3>
 * <p>
 * This class will be called at the start in the Launcher, and will start the user interface process
 * </p>
 *
 * <h3>Usage</h3>
 * <p>
 * This class should be used as follows:
 *
 * <pre>
 * <code>${type_name} instance = new ${type_name}();</code>
 * </pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 *         ${tags}
 */
public class MainPanel 
{
	private String
	userInputID,
	userInputPassword;
	
	private Scanner userInput;
	private boolean loginSuccessful;
	
	/*
	 * Constructor
	 * */
	public MainPanel() 
	{	
		startLoginProcess();
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>This method will start the login process</p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void startLoginProcess() 
	{	
		userInput = new Scanner(System.in);
		
		System.out.println("-- *Welcome To The DataBase* --");
		
		//ask for login details
		askToLogin();
		
		//will loop until the user put correct id and password
		for(int i = 0; i < 3;) 
		{
			//if the login details are entered correctly
			if(isLoginCorrect()) 
			{
				System.out.println("\nPlease Wait..");
				
				waitForSecond(2);
				
				System.out.println("\nLogin In Successful..");
				
				waitForSecond(1);
				
				//allow user to proceed to the next phase
				loginSuccessful = true;
				
				break;
			}
			
			//if the login details are not entered correctly
			else 
			{
				//if the user have made more than 3 attempts..
				if(i >= 2)
				{
					System.out.println("\n(attempt 3 of 3)");
					System.out.println("\nAccess Denied!\nYou have entered an invalid login & have reached the limit");
					loginSuccessful = false;
					break;
				}
				else 
				{
					i++;
					System.out.println("\n(attempt "+(i)+" of 3)");
					System.out.println("\nAccess Denied! Please Try Again!");
					askToLogin();	
				}
			}
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * following method is used to check if the user have entered login details correctly
	 * returns true if login details are correct else false
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private boolean isLoginCorrect() 
	{
		return userInputID.equals(ConfigurationService.getProperty(ConfKey.DB_USER)) && userInputPassword.equals(ConfigurationService.getProperty(ConfKey.DB_PASSWORD));
	}

	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * following method will be used used for asking the login details from the user
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void askToLogin() 
	{
		askForID();
		userInputID = userInput.next();
		askForPassword();
		userInputPassword = userInput.next();
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * following method will display a text to ask user to input the id
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void askForID() 
	{
		System.out.println("\nPlease Enter User ID");
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * following method will display a text to ask user to input the password
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void askForPassword() 
	{
		System.out.println("\nPlease Enter Password");
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * <p>This method allows put a delay between certain operations </p>
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	private void waitForSecond(long sec) 
	{
		try 
		{
			TimeUnit.SECONDS.sleep(sec);
		}
		catch (InterruptedException e) 
		{
			System.out.println("Unable to proceed. Please contact technical support");
			e.printStackTrace();
		}		
	}

	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * <p>According to this return method, it will be decided, 
	 * whether or not user should proceed to next phase</p>
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public boolean isLoginSuccessful() 
	{
		return loginSuccessful;
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * <p>This method will display the options that
	 * which user could use to get information or/and to do adjustment in the database</p>
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 */
	public void displayDatabaseOptions() 
	{
		System.out.println("\nPlease choose one of the following options\nEnter the number of the option you want to proceed with");
		System.out.println("1.Create an Identity");
		System.out.println("2.Delete an Identity");
		System.out.println("3.Search an Identity");
		System.out.println("4.Replace/Edit an Identity");
		System.out.println("5.Exit");
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will comes in after the login process 
	 * and will show the operation that the user can perform in the database
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */

	public void ActivateDatabase() throws Exception 
	{
		for(int i = 0;;) 
		{	
			String option = userInput.next();
			
			if(option.equals("1")) 
			{
				createIdentity();
			}
			else if(option.equals("2")) 
			{
				deleteIdentity();
			}
			else if(option.equals("3")) 
			{
				searchIdentity();
			}
			else if(option.equals("4")) 
			{
				updateIdentity();
			}
			else if(option.equals("5")) 
			{
				signout();
				break;
			}
			else 
			{
				System.out.println("\nInvalid input\n");
				displayDatabaseOptions();
			}
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This will handle the option of creating new identity in the database as according to the information provided by the user
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void createIdentity() throws Exception 
	{
		final IdentityDAO dao = new IdentityXMLDAO();
		
		String name, uid, email;
		
		System.out.println("\nYou have selected to create new identity");
		
		name = getInfromationFromUser("Please provide the Name of personal");
		uid = getInfromationFromUser("Please provide the User ID");
		email = getInfromationFromUser("Please provide the Email");
		
		final Identity newIdentity = new Identity(name, uid, email);
		System.out.println("\nPlease wait..");
		System.out.println("\nCreating new Identity & adding it to database..");
		waitForSecond(2);
		try 
		{
			dao.create(newIdentity);			
			System.out.println("\nNew Identity added to the database successfully!");	
			displayDatabaseOptions();
		}
		catch (EntityCreationException e) 
		{
			System.out.println("\nSorry, we are unable to create new identity\nPlease contact technical support!");
			displayDatabaseOptions();
			e.printStackTrace();
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will handle the option of deleting certain identity as according to the information provided by the user 
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void deleteIdentity() throws Exception 
	{
		final IdentityDAO dao = new IdentityXMLDAO();
		
		String name, uid, email;
		
		System.out.println("\nYou have selected to remove an identity from database");
		
		name = getInfromationFromUser("Please provide the Name of personal");
		uid = getInfromationFromUser("Please provide the User ID");
		email = getInfromationFromUser("Please provide the Email");
		
		final Identity unecessaryIdentity = new Identity(name, uid, email);
		System.out.println("\nPlease wait..");
		System.out.println("\nRemoving Identity from database..");
		waitForSecond(2);
		try 
		{
			dao.delete(unecessaryIdentity);			
			System.out.println("\nIdentity removed successfully from database!");	
			displayDatabaseOptions();
		}
		catch (EntityDeletionException e) 
		{
			System.out.println("\nSorry, we are unable to locate the identity\nPlease, try again!\nOR\ncontact technical support!");
			displayDatabaseOptions();
			e.printStackTrace();
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will handle the option of search certain identity as according to the information provided by the user 
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void searchIdentity() throws Exception 
	{
		final IdentityDAO dao = new IdentityXMLDAO();
		
		ConfKey key = null;
		
		String option = null, name = null, uid = null, email = null;
		
		System.out.println("\nYou have selected to search an identity");

		displaySearchOptions();
		
		try 
		{
			option = userInput.next();			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		for(int i = 0;;) 
		{
			switch(option) 
			{
				case "1":
					name = getInfromationFromUser("Please provide the Name of personal");
					key = ConfKey.SEARCH_BY_NAME;
					break;
				case "2":
					uid = getInfromationFromUser("Please provide the User ID");
					key = ConfKey.SEARCH_BY_UID;
					break;
				case "3":
					email = getInfromationFromUser("Please provide the Email");
					key = ConfKey.SEARCH_BY_EMAIL;
					break;
				case "4":
					name = getInfromationFromUser("Please provide the Name of personal");
					uid = getInfromationFromUser("Please provide the User ID");
					key = ConfKey.SEARCH_BY_NAME_AND_UID;
					break;
				default:
					System.out.println("\nInvalid input\n");
					displaySearchOptions();	
					option = null;
					option = userInput.next();
					break;
			}
			
			if(key != null)
				break;
		}
		
		final Identity sIdentity = new Identity(name, uid, email);
		
		System.out.println("\nPlease wait..");
		System.out.println("\nSearching identity...");
		waitForSecond(2);		
		try
		{
			final List<Identity> resultList = dao.search(sIdentity, key);
			
			try 
			{
				if (resultList.size() <= 0) 
				{
					System.out.println("\nSorry, we are unable to search the identity\nPlease contact technical support!");
					displayDatabaseOptions();
					return;
				}			
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			System.out.println(resultList);
			System.out.println("search successful!"); 
			displayDatabaseOptions();
		}
		catch (Exception e) 
		{
			System.out.println("\nSorry, we are unable to search the identity\nPlease contact technical support!");
			displayDatabaseOptions();
			e.printStackTrace();
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will display the options with in the search option
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void displaySearchOptions() 
	{
		System.out.println("\nPlease provide some detail of the personal you are looking for");
		System.out.println("\nEnter the number of the option you want to proceed with");
		System.out.println("1.Search by Name");
		System.out.println("2.Search by User ID");
		System.out.println("3.Search by email");
		System.out.println("4.Search by Name & User ID");	
	}

	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will return the input provided by the user
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private String getInfromationFromUser(String askUser) 
	{	
		System.out.println("\n"+askUser);
		String infromation = userInput.next();
		
		return infromation;
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will handle the option of update/replace/edit certain identity as according to the information provided by the user 
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void updateIdentity() throws Exception 
	{
		final IdentityDAO dao = new IdentityXMLDAO();
		
		String name = null, uid = null, email = null;
		String updatedName = null, updatedEmail = null, updatedUID = null;
		
		System.out.println("\nYou have selected to update an identity in the database");
		
		name = getInfromationFromUser("Please provide the Name of personal you want to replace");
		uid = getInfromationFromUser("Please provide the User ID you want to replace");
		email = getInfromationFromUser("Please provide the Email you want to replace");
		
		final Identity unecessaryIdentity = new Identity(name, uid, email);
		System.out.println("\nPlease wait..");
		System.out.println("\nSearching Identity ");
		
		waitForSecond(2);
		
		try 
		{			
			System.out.println("\nIdentity found!");	
			
			updatedName = getInfromationFromUser("Please provide new Name");
			updatedUID = getInfromationFromUser("Please provide new User ID");
			updatedEmail = getInfromationFromUser("Please provide new Email");
			final Identity updatedIdentity = new Identity(updatedName, updatedUID, updatedEmail);
			System.out.println("\nPlease wait..");
			waitForSecond(2);
			
			dao.update(unecessaryIdentity, updatedIdentity);
			System.out.println("\nIdentity updated successfully");
			displayDatabaseOptions();
		}
		catch (Exception e) 
		{
			System.out.println("\nSorry, we are unable to locate and update the identity\nPlease, try again!\nOR\ncontact technical support!");
			displayDatabaseOptions();
			e.printStackTrace();
		}
	}
	
	/** 
	 * <h3>Description</h3>  
	 * <p>
	 * This method will handle the signing out process
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>It should be used as follows :
	 *   
	 * <pre><code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code></pre>
	 * </p>
	 *  
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 * ${tags}
	 * @throws Exception 
	 */
	private void signout() 
	{
		System.out.println("\nPlease wait..");
		System.out.println("\nSigning out..");
		waitForSecond(2);
		System.out.println("\nSign out successful!");
		System.out.println("\nThank you for useing the database");
	}
}
