/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant :
 */
package fr.epita.database.services.identity;

import fr.epita.database.datamodel.Identity;
import fr.epita.database.services.DAO;

/**
 * <h3>Description</h3>
 * <p> This is an extension of DAO where collection of abstract methods gets extended with another method
 * </p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public interface IdentityDAO extends DAO<Identity> 
{

	/**
	 * <h3>Description</h3>
	 * <p>
	 * This methods allows to ...
	 * </p>
	 *
	 * <h3>Usage</h3>
	 * <p>
	 * It should be used as follows :
	 * 
	 * <pre>
	 * <code> ${enclosing_type} sample;
	 *
	 * //...
	 *
	 * sample.${enclosing_method}();
	 *</code>
	 * </pre>
	 * </p>
	 * 
	 * @since $${version}
	 * @see Voir aussi $${link}
	 * @author ${user}
	 *
	 *         ${tags}
	 */
	boolean healthCheck();

}
