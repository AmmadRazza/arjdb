/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant :
 */
package fr.epita.database.exceptions;

/**
 * <h3>Description</h3>
 * <p>This class allows to handle exception related to XML doc</p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public class DocXMLException extends Exception 
{
	public DocXMLException(Exception e) throws Exception 
	{
		throw new Exception(getUserMessage());
	}

	public String getUserMessage() 
	{
		return "There has been some problem\\nPlease Try Again Later!\\nContact customer support";
	}

}
