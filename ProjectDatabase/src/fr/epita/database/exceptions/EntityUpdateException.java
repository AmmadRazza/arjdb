/**
 * Ce fichier est la propriété de Ammad Raza
 * Code application :
 * Composant : 
 */
package fr.epita.database.exceptions;

import fr.epita.database.datamodel.Identity;

/** 
 * <h3>Description</h3>  
 * <p>This class allows to handle exception related to update of an identity</p>
 *
 * <h3>Usage</h3>
 * <p>This class should be used as follows:
 *   <pre><code>${type_name} instance = new ${type_name}();</code></pre>
 * </p>
 *  
 * @since $${version}
 * @see See also $${link}
 * @author ${user}
 *
 * ${tags}
 */
public class EntityUpdateException extends Exception 
{

	final Object entity;

	/**
	 *
	 */
	
	public EntityUpdateException(Object entity, Throwable cause) 
	{
		this.entity = entity;
		initCause(cause);
	}
	
	public String getUserMessage() 
	{
		return "the following entity creation has failed :" + entity.toString();
	}

}
